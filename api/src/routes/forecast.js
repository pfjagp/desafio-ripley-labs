import "@babel/polyfill";
import express from 'express';
import bodyParser from 'body-parser';
import DarkSky from 'dark-sky';
import { getClient } from '../bin/redis';
import WebSocket from 'ws';
import { TYPE_REQUEST } from '../common/constants';


const wss = new WebSocket.Server({ port: 8888 })


var router = express.Router();
var jsonParser = bodyParser.json()


const darksky = new DarkSky('4eac439895a3143771f54c65249a72e5')

console.log(process.env.DARK_SKY)

router.use(jsonParser);

wss.on('connection', ws => {
    ws.on('message', message => {
        let data = []

        try {
            //data = getDataForecast(TYPE_REQUEST.URL_SOCKET);
        }
        catch (e) {
            //saveErrors(e)
            //data = getDataForecast(TYPE_REQUEST.URL_SOCKET);
        }
        console.log(`Received message => ${message}`)
        ws.send(JSON.stringify({ response: data }))

    })
    console.log(`connection`)
    ws.send(JSON.stringify({ response: 'connection' }))
})

const forecastRequest = async (latitude, longitude) => {

    try {
        const forecast = await darksky
            .options({
                latitude,
                longitude,
                language: 'es'
            })
            .exclude('daily,hourly,minutely,flags')
            .get()
        return forecast;
    } catch (err) {
        console.log(err, 'err')
        return false
    }
};

const getDataForecast = (type, res) => {
    const numR = Math.random(0, 1)
    console.log(numR)
    const key = "coordinates_values";
    const client = getClient();
    if (numR < 0.1) { throw new Error('How unfortunate! The API Request Failed') }
    client.get(key, async function (error, result) {

        let response = [];
        let coordinates = [];


        if (error) {
            console.log(error);
            throw error;
        }
        coordinates = JSON.parse(result)


        for (const item of coordinates) {
            try {
                const dataForecast = await forecastRequest(item.latitude, item.longitude);
                const { currently } = dataForecast
                const coordinate = {
                    name: item.name,
                    latitude: dataForecast.latitude,
                    longitude: dataForecast.longitude,
                    temperature: currently.temperature,
                    timezone: dataForecast.timezone,
                };

                response.push(coordinate);
            }
            catch (e) {
                throw e;
            }

        }
        if (type === TYPE_REQUEST.API) {
            res.status(200);
            res.send(response);
        }
        else {
            return response
        }

    });
}

const saveErrors = (e) => {
    const client = getClient();
    const hash = "api.errors"
    const key = Date.now()

    client.hmset(hash, {
        [`${key}`]: e.toString(),
    });
    console.log('ERROR --', e)
}

router.get('/', async (_, res) => {

    try {
        getDataForecast(TYPE_REQUEST.API, res);
    }
    catch (e) {
        saveErrors(e);
        getDataForecast(TYPE_REQUEST.API, res);

    }

});





router.get('/:latitude/:longitude', async (req, res) => {
    const { latitude, longitude } = req.params
    let response = false
    while (response === false) {
        try {
            response = await forecastRequest(latitude, longitude);
        }
        catch (e) {
            response = false;
        }
    }

    res.status(200);
    res.send(response);

});

export default router;


