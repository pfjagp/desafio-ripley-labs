
import config from './config';
import coordinates from '../common/constants'

var redis = require("redis"),
    client = redis.createClient({
        host: config.get('redis.host')
    });

client.on('connect', function () {
    console.log('Redis client connected');
    const key = "coordinates_values";

    saveValuesInRedis(key, coordinates);

});

client.on("error", function (err) {
    console.log("Error " + err)
});

export const saveValuesInRedis = (key, body) => {
    const formatBody = JSON.stringify(body);
    client.set(key, formatBody, redis.print);
}

export const getClient = () => client

const redisMiddleware = (req, res, next) => {


    let key = "__expIress__" + req.originalUrl || req.url;

    client.get(key, function (err, reply) {

        if (reply) {
            res.send(JSON.parse(reply));
        } else {
            res.sendResponse = res.send;
            res.send = (body) => {
                try {
                    saveValuesInRedis(key, body);
                } catch (e) {
                    console.log("ERROR", e)
                }

                res.sendResponse(body);
            }

            next();
        }
    });
};



export default redisMiddleware;
