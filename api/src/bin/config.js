import convict from 'convict';
import * as fs from 'fs';

// Define a schema
var config = convict({
    env: {
        doc: "The application environment.",
        format: ["production", "development", "test"],
        default: "development",
        env: "NODE_ENV"
    },
    redis: {
        host: {
            doc: "Redis host",
            format: String,
            default: 'redis',
            env: 'REDIS_HOST'
        },
    }
});

// Load environment dependent configuration
var env = config.get('env');
const envFile = './config/' + env + '.json'
if (fs.existsSync(envFile)) {
    config.loadFile(envFile);
}

// Perform validation
config.validate({ allowed: 'strict' });

module.exports = config;