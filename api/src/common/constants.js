const coordinates = [{
    name: 'Santiago',
    latitude: "-33.4372",
    longitude: "-70.6506",
},
{
    name: 'Zurich',
    latitude: "47.373689",
    longitude: "8.542203"
},
{
    name: 'Auckland',
    latitude: "-36.8404",
    longitude: "174.74"
},
{
    name: 'Sydney',
    latitude: "-33.8667",
    longitude: "151.2"
},
{
    name: 'Londres',
    latitude: "51.5072",
    longitude: "-0.1275"
},
{
    name: 'Georgia',
    latitude: "31.890485",
    longitude: "-83.339001"
}];

export default coordinates

export const TYPE_REQUEST = {
    API: 'API',
    SOCKET: 'SOCKET',
}