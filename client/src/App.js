import React from 'react';
import './App.css';
import Constants from './common/Constants'
import { SimpleMap } from './components/Maps'



class App extends React.PureComponent {
  ws = new WebSocket(Constants.URL_SOCKET)

  state = {
    coordinates: []
  }

  fetchCoordinates() {
    fetch(`${Constants.URL}/forecast/`)
      .then(response => response.json())
      .then(coordinates => this.setState({ coordinates }))
      .catch(err => err);

  }

  handlerSocket() {
    this.ws.onopen = () => {
      // on connecting, do nothing but log it to the console
      console.log('connected')
      setInterval(() => this.ws.send('refresh'), 10000);
    }

    this.ws.onmessage = evt => {
      // on receiving a message, add it to the list of messages
      const message = JSON.parse(evt.data)
      console.log(message)


    }

    this.ws.onclose = () => {
      console.log('disconnected')
      // automatically try to reconnect on connection loss
      this.setState({
        ws: new WebSocket(Constants.URL_SOCKET),
      })
    }
  }




  componentDidMount() {
    this.fetchCoordinates()
    this.handlerSocket()

  }



  render() {
    const { coordinates } = this.state
    return (
      <div className="App">
        <SimpleMap coordinates={coordinates} />
      </div>
    );
  }


}

export default App;
