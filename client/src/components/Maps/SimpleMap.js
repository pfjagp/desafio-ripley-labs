import React from 'react'
import { GoogleApiWrapper, Marker, Map, InfoWindow } from 'google-maps-react';
import { isEmpty } from 'lodash';
import { FaGlobeAmericas, FaThermometerThreeQuarters, FaCalendar, FaClock } from 'react-icons/fa';

class SimpleMap extends React.Component {
    state = {
        activeMarker: {},
        selectedPlace: {},
        showingInfoWindow: false,
    }

    shouldComponentUpdate(_, nextState) {
        if (isEmpty(this.state.selectedPlace)) {
            return true
        }
        return nextState.selectedPlace.name !== this.state.selectedPlace.name;
    }

    onMarkerClick = (props, marker) =>
        this.setState({
            activeMarker: marker,
            selectedPlace: props,
            showingInfoWindow: true
        });

    onInfoWindowClose = () =>
        this.setState({
            activeMarker: null,
            showingInfoWindow: false
        });


    render() {
        const { coordinates } = this.props

        return (

            <div style={{ height: '100vh', width: '100%' }}>
                <Map
                    className="map"
                    google={this.props.google}
                    onClick={this.onMapClicked}
                    style={{ height: '100%', position: 'relative', width: '100%' }}
                    zoom={2} zoomControl={false} scrollwheel={false}>


                    {
                        coordinates.map(coordinate => {
                            let time = new Date().toLocaleString("en-US", { timeZone: coordinate.timezone });
                            time = new Date(time);
                            const { 0: date, 1: hour } = time.toLocaleString().split(',')
                            return (<Marker key={coordinate.name} temperature={coordinate.temperature} date={date} hour={hour} name={coordinate.name} position={{ lat: `${coordinate.latitude}`, lng: `${coordinate.longitude}` }} onMouseover={this.onMarkerClick} onClick={this.onMarkerClick} />)
                        })
                    }



                    <InfoWindow
                        marker={this.state.activeMarker}
                        onClose={this.onInfoWindowClose}
                        visible={this.state.showingInfoWindow}>
                        <div>
                            <h1 style={{ borderBottomStyle: 'solid', borderColor: '#43acf5' }} > <FaGlobeAmericas /> {this.state.selectedPlace.name}</h1>
                            <h2><FaCalendar /> {this.state.selectedPlace.date}</h2>
                            <h2><FaClock /> {this.state.selectedPlace.hour}</h2>
                            <h2><FaThermometerThreeQuarters /> {this.state.selectedPlace.temperature} °F</h2>


                        </div>
                    </InfoWindow>

                </Map>
            </div>
        )
    }
}

export default GoogleApiWrapper({
    apiKey: ('AIzaSyB1GtMtjtNXF1LNh9nyHgiBp4-Adn6whbU')
})(SimpleMap)
