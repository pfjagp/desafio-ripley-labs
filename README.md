
## Deploy APP

You�ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Instalar Docker y Docker Compose
2. Clonar el codigo del respositorio
3. Configurar el archivo .env del directorio raiz con la variable APP_HOST con la ip o url donde se vaya a realizar el deploy del app, si estas en localhost recomiendo el siguiente DNS app.test.localhost.tv http://localhost.tv/ te dejo el link por si no conoces de que se trata. 
4. ir a la carpeta raiz del repositorio 
5. Ejecutar el comando docker-compose up -d 
6. Si estas en tu localhost visita http://app.test.localhost.tv/ luego de un par de minutos y veras la aplicacion funcionando, si lo haces en una instancia dirigite a la url que colocaste en el paso numero 3.


---
## Ejemplo en vivo
http://ec2-54-174-37-201.compute-1.amazonaws.com/
---

